spice-space.org Pages
#####################

.. image:: https://gitlab.freedesktop.org/spice/spice-space-pages/badges/master/build.svg
  :target: https://gitlab.freedesktop.org/spice/spice-space-pages/commits/master
  :alt: GitLab CI build status

spice-space.org Pages are used by pelican to generate the SPICE project website
https://www.spice-space.org

Contributing
++++++++++++

Do you want to contribute ? Send a patch to spice-devel@lists.freedesktop.org !
You can also create a Merge Request on GitLab.
