Nightly builds
###############################################################################

:slug: nightly-builds
:modified: 2017-02-14 17:00

.. |spice-protocol| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-protocol/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-protocol/
.. |spice-server| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice/
.. |spice-vdagent| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-vdagent/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-vdagent/
.. |spice-gtk| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-gtk/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/spice-gtk/
.. |virt-viewer| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/virt-viewer/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/virt-viewer/
.. |usbredir| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/usbredir/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/usbredir/
.. |phodav| image:: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/phodav/status_image/last_build.png
                        :target: https://copr.fedorainfracloud.org/coprs/g/spice/nightly/package/phodav/

Do you want to test the latest stuff ? It is easy if you are running RHEL,
Fedora or CentOS.

Check out our copr repo https://copr.fedorainfracloud.org/coprs/g/spice/nightly/
which contains rpms generated from the git master of our projects.

You can quickly enable it by:

.. code-block:: sh

    dnf copr enable @spice/nightly

Build status
+++++++++++++

============== ================
spice-protocol |spice-protocol|
spice-server   |spice-server|
spice-vdagent  |spice-vdagent|
spice-gtk      |spice-gtk|
virt-viewer    |virt-viewer|
usbredir       |usbredir|
phodav         |phodav|
============== ================
