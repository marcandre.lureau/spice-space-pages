Spice Proxy
###########

:slug: spice-proxy

Introduction
++++++++++++

Spice client (remote-viewer) supports connecting to the server via an http proxy.
This may be desirable for cases when the client does not have direct access
to the server.

Configuring the Client
++++++++++++++++++++++

Proxy Format
^^^^^^^^^^^^
[protocol://]proxy-host[:proxy-port]

.. code-block:: sh

   for example: http://10.0.15.50:3128

There are two ways to tell the client to connect via an http proxy:

1. SPICE_PROXY environment variable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A SPICE_PROXY environment variable tells remote-viewer
to connect to the spice-server via a proxy-server

.. code-block:: sh

   export SPICE_PROXY="http://10.0.15.50:3128"

(Please do not use "http_proxy" as it is not currently supported)

2. proxy key in a vv-file (under [virt-viewer])
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A proxy key in a vv-file tells remote-viewer to
connect to the spice-server via a proxy-server

.. code-block:: sh

   [virt-viewer]
   proxy=http://10.0.15.50:3128


Configuring the proxy server (squid as an example)
++++++++++++++++++++++++++++++++++++++++++++++++++
Squid (squid-cache.org) can be used as a proxy server.

This is just an example.
There are other configurations possible, and other proxy
servers.
Configuration should be done according to requirements.
Firewall, if exists, may need to be configured as well.


Example Configuration (Fedora)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
For information about configuring Squid, please take a look
at squid documentation.
I looked at http://wiki.squid-cache.org/SquidFaq/SquidAcl.

Let's assume there are two hosts (hypervisors) with
IP addresses 10.0.0.1 and 10.0.0.2, and both
use ports 5900 and 5901 for Spice.
A possible configuration may be (in /etc/squid/squid.conf):

.. code-block:: sh

   acl SPICE_HOSTS 10.0.0.1 10.0.0.2
   acl SPICE_PORTS 5900 5901
   http_access allow SPICE_HOSTS
   http_access allow SPICE_PORTS
   http_access deny all

allow these hosts and ports but nothing else.


Running the client
++++++++++++++++++++++
Once the proxy is set up as described above, run the client as usual, e.g

.. code-block:: sh

   remote-viewer console.vv

or

.. code-block:: sh

   remote-viewer spice://10.0.0.1:5901
