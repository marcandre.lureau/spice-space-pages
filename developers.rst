Developers
###############################################################################

.. _Spice User Manual: spice-user-manual.html
.. _spice for newbies: spice-for-newbies.html
.. _spice protocol: spice-protocol.html
.. _VD-Interfaces: vd-interfaces.html
.. _spice-html5: https://gitlab.freedesktop.org/spice/spice-html5
.. _spice style: spice-project-coding-style-and-coding-conventions.html
.. _xf86-video-qxl: http://cgit.freedesktop.org/xorg/driver/xf86-video-qxl
.. _style: http://cgit.freedesktop.org/pixman/tree/CODING_STYLE
.. _bug-tracker: https://bugs.freedesktop.org/enter_bug.cgi?product=Spice
.. _feature-tracker: https://bugs.freedesktop.org/enter_bug.cgi?product=Spice&component=RFE
.. _Future Features list:
.. _Features: features.html
.. _documentation: documentation.html

New to Spice?
+++++++++++++

Spice project provides documentation that will help you get familiar with Spice.
Start by reading `Spice For Newbies`_ for getting information about Spice basic
architecture and Spice components. You can use `Spice user manual`_ for information
on how to get, make and use Spice. Read `SPICE Protocol`_ and `VD-Interfaces`_ which
contain additional and more specific information.

Like to get involved?
+++++++++++++++++++++

The Spice project is open for contribution. As the project activity is spread
across many areas of interest, a variety of C/C++ programmers will find areas of
interest. Javascript/web developers should checkout `spice-html5`_.

The project is devoted to offering increased reliability, quality and usability.
If you choose to get involved, you are obligated to keep Spice project
standards. Commit and forget style is unacceptable; code maintenance and bug
fixing is part of the trade.

Still like to get involved?
+++++++++++++++++++++++++++

You can start by reading the Spice documentation_ and
the `Spice style`_ document (`xf86-video-qxl`_ is under another `style`_). Browse Spice
`feature-tracker`_ and Spice `bug-tracker`_ in order to find a task. You can also post
to spice-devel@lists.freedesktop.org the type of task you are looking for, so
maybe someone have just the right task for you. You can also refer to the `Future Features list`_. Before starting to work on some new feature or a big
change, coordinate it with Spice project members. Doing so will reduce the
chance that you'll do a duplicate work, while it will also reduce the chance
that your patch will get rejected.

Like to send a patch?
+++++++++++++++++++++

Before sending a patch to spice-devel@lists.freedesktop.org using **git send-email**
make sure that it follows these guidelines:

- It applies and compiles correctly with the latest development version
- The code adheres to Spice coding convention and style as specified in `Spice
  style`_
- Split a large patch to multiple smaller patches, each having a meaningful
  logical purpose. It will help in the process of reviewing and accepting your
  patch. Make sure applying each patch does not break the build
- Write clear and meaningful description and explanation in the commit message
- Be as responsive as possible to the review comments
- The patch was prepared using **git format-patch**
- The patch successfully compiles on all supported platforms

Like to send a patch but don't have time for all the “nonsense”?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If it is a simple bug fix, send the patch to spice-devel@lists.freedesktop.org
and specify that you don't care what we do with the patch. We will do with it
whatever seems appropriate.

For more developers content, visit `Features`_.
