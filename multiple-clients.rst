Simultaneous clients connection
###############################################################################

.. role:: bash(code)
   :language: bash

:slug: multiple-clients
:modified: 2016-01-06 15:20

Support multiple concurrent connections to a single spice server.

Status
++++++

This feature is still experimental, it is not expected to work correctly under different client bandwidths, although it should not crash the server.

To enable:

Using libvirt
^^^^^^^^^^^^^

Modify the xml definition of the virtual machine to use :bash:`SPICE_DEBUG_ALLOW_MC` environment variable:

.. code-block:: XML

  <domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
  ....
    <qemu:commandline>
      <qemu:env name='SPICE_DEBUG_ALLOW_MC' value='1'/>
    </qemu:commandline>
  </domain>

Using QEMU
^^^^^^^^^^

:bash:`export SPICE_DEBUG_ALLOW_MC=1`

Launch qemu vm as usual.

The stdout will contain an additional line:

:bash:`spice: allowing multiple client connections`

You can then launch clients as usual. A second client connection will not disconnect any of the previous clients. There is no set limit. The behavior of each channel in multiple client mode:

- inputs - shared. All clients can supply mouse and keyboard.
- display - shared. All clients receive display updates.
- cursor - shared. All clients receive cursor updates.
- playback - first connection after no connections. To do: make it shared.
- record - first connection after no connections. To do: make it shared.
- smartcard - first connection after no connections. To do: make it shared.
- usbredir - first connection after no connections. To do: make it shared.
- agent - ?

NB: The main channel is not on that list because it isn't a user visible channel. Every client has it's own main connection.

NB: First connection after no connections: To receive this channel you will have to connect after explicitly disconnecting all the previous clients. Otherwise you will easily reach a situation where ''none'' of the clients have any of those channels:

.. code-block:: sh

 connect A
 connect B (A is connected)
 disconnect A (B is still connected)
 connect C

Now both B and C only have {inputs,display,cursor}, no one has any other (playback,cursor,smartcard) connection. Specifically, the server didn't advertise those channels to clients B and C.
