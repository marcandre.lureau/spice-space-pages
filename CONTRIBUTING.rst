Contributing
############

Do you want to contribute ? Send a patch to spice-devel@lists.freedesktop.org !
You can also create a Merge Request on GitLab.

In case you have the commit access to the spice-space git repository
https://gitlab.freedesktop.org/spice/spice-space-pages.git and gitlab ci tests passed
for your commits, you can push. The website will be updated automatically
in a few hours
